# FreeBSD CI

Tooling to setup FreeBSD CI builders.

## Initial builder setup

### Enable the ci-repo

The default FreeBSD package repository needs to be disabled, and be replaced by the one specifically built with debug symbols.

1) Create the repository directory
```
mkdir -p /usr/local/etc/pkg/repos/
```
2) To disable the offical repo create the file `/usr/local/etc/pkg/repos/FreeBSD.conf` with the contents
```
FreeBSD { enabled: no }
```
3) Add the ci-repo by creating the file `/usr/local/etc/pkg/repos/ci_pkgs.conf` with contents
```
ci_pkgs: {
        url: "https://cdn.kde.org/freebsd/132amd64",
        signature_type: "none",
        enabled: yes,
        priority: 10
}
```
4) Bootstrap the repository by running 
```
pkg update
```


### Installation of packages

This repositories script can hopefully be used to simplify this process. It relies on the docker files for the OpenSuse builders.

Clone this repo to the builder and run 
```
./install_pkgs.rb
```


## Contents of 'package-map.json'

This files contains the configuration used by the `install_pkgs.rb` script.

The json object has the following keys:

* `docker_sources`: these are the urls of the docker files for the OpenSuse builders -- at the moment it those are `suse-qt515` and `suse-qt65`.
The script searches the `zipper in` and `pip install` lines and maps the packages specified there to FreeBSD packages, which it then will install.

* `default_versions`: FreeBSD ports come with multiple versions of the same software. This map specifies the versiosn we want.
At the moment this includes for example specifies that we want `llvm15` and `gcc12`.

* `base_packages`: this is a list of packages that should be installed irregardless of what we parse out of the docker files. This includes for example `git` and the metaports for `qt5` and `qt6`.

* `package_map`: this is a map to get from OpenSuse package names to (possibly a list) of FreeBSD package names. The mapped values may included `%%version%%` where `version` should be specified in `default_versions` above.

* `ignored_packages`: some packages may not exist in the FreeBSD world -- for example `systemd`. All packages that we know we do not want, should be listed here.
The output of the script will list the packages it could not map -- ideally the ones we can provide should be provided, and the ones we cannot should be amended to this list.


## Adding additional dependencies

To add further packages, just add them either to `base_packages` or wait for the referenced docker-images to included their names.
If the names in the docker-images do not match the ones in FreeBSD, also add an entry in the `packages_map` hash.
After that, update the checkout, and rerun `./install_pkgs.rb`.

If the package is not yet present in the repository add it to the `ports-list.txt` file in this repository.
The entries in `ports-list.txt` are simply `category/port(@flavor)?` origins of the packages. 

A cron job will regularily pull this file and update the repository.
