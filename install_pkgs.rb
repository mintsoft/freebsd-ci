#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'
require 'set'
require 'open3'
require 'open-uri'

PACKAGE_MAP = 'package-map.json'

class DockerReader
  ZYPPER_REGEX = /^.*zypper.*--non-interactive in --allow-vendor-change(.*)$/.freeze
  PIP_REGEX = /^.*pip install --break-system-packages(.*)$/.freeze

  attr_reader :missing, :found, :config

  def initialize(config_file)
    @config = JSON.parse(File.read(config_file))

    @missing = Set.new
    @found   = Set.new

    load!
  end

  def status
    print_lines = lambda { |lines|
      count = lines.count
      fmt   = ->(number) { return number.to_s.rjust(count.to_s.length) }
      idx = 0
      lines.sort.each do |line|
        puts "[#{fmt[idx += 1]}/#{count}]\t#{line}"
      end
    }

    puts '=== Ignoring the following OpenSuse packages'
    print_lines[config['ignored_packages']]

    puts '=== Could not map the following OpenSuse packages to FreeBSD'
    print_lines[missing]

    puts '=== Mapped the following OpenSuse packages to FreeBSD'
    print_lines[found]
  end

  def install!
    puts "=== Running pkg-install on #{found.count} packages"
    std_out, std_err, status = Open3.capture3('pkg', 'install', '-y', *found.to_a)
    if status.success?
      puts std_out
    else
      puts std_err
    end
  end

  private

  def load!
    package_list = transform_packages(config['base_packages'])

    config['docker_sources'].each do |url|
      package_list += transform_packages(load_zipper_packages(url))
    end

    pkg_origin = lambda { |name|
      std_out, _, status = Open3.capture3('pkg', 'search', '--quiet', '--search', 'name', '--exact', name)
      return std_out.split("\n").uniq.first if status.success?

      return nil
    }
    package_list.each do |package|
      if (origin = pkg_origin[package])
        found << origin
      else
        missing << package
      end
    end
  end

  def load_zipper_packages(url)
    run_lines = URI.open(url, &:read)
                   .gsub(/#.*$/, '\\') # remove comments
                   .gsub("\\\n", ' ')  # remove line continuations
                   .split("\n")
                   .grep(/RUN/)

    map_package_name = ->(name) { return config['package_map'][name] || name }

    transform_lines = lambda { |regex|
      # yes, this is not performant, but compared to the pkg-calls it is still fast
      return run_lines.grep(regex)
                      .map { |line| line.gsub(regex, '\1').strip.split(' ') } # extract the zypper install commands
                      .flatten
                      .map { |name| name.gsub(/-devel$/, '') } # do some intial sanitization
                      .sort
                      .uniq
                      .map { |name| map_package_name[name] } # map to freebsd pkg names
                      .flatten
                      .reject { |name| config['ignored_packages'].include?(name) } # remove the ones we do not care about
    }

    # this now contains a list of FreeBSD packages to be installed
    transform_lines[ZYPPER_REGEX] + transform_lines[PIP_REGEX]
  end

  def transform_packages(list)
    map_default_version = lambda { |name|
      config['default_versions'].each do |version, value|
        name = name.gsub("%%#{version}%%", value)
      end

      return name
    }

    # sed in the version numbers
    list.map { |name| map_default_version[name] }
  end
end

packages = DockerReader.new(File.join(File.expand_path(File.dirname(__FILE__)), PACKAGE_MAP))
packages.status
packages.install!
